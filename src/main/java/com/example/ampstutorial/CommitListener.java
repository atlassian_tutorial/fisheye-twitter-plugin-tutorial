package com.example.ampstutorial;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.fecru.user.User;
import com.atlassian.fisheye.event.CommitEvent;


import com.atlassian.fisheye.spi.data.ChangesetDataFE;
import com.atlassian.fisheye.spi.services.RevisionDataService;
import com.cenqua.fisheye.model.manager.CommitterUserMappingManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.PropertyConfiguration;

import java.util.Properties;

/**
 * Listener that listens to every commit to a repository and sends a twitter update with the changeset description.
 * Every user's tweets are sent to individual accounts.
 * Requires user's Twitter credentials to be available in {@link TwitterLoginRecordStore}.
 */
@Component
public class CommitListener implements InitializingBean, DisposableBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(CommitListener.class);

    private final EventPublisher eventPublisher;
    private final CommitterUserMappingManager committerUserMappingManager;
    private final RevisionDataService revisionDataService;
    private final TwitterLoginRecordStore twitterLoginRecordStore;

    @Autowired
    public CommitListener(EventPublisher eventPublisher,
                          CommitterUserMappingManager committerUserMappingManager,
                          RevisionDataService revisionDataService,
                          TwitterLoginRecordStore twitterLoginRecordStore) {
        this.eventPublisher = eventPublisher;
        this.committerUserMappingManager = committerUserMappingManager;
        this.revisionDataService = revisionDataService;
        this.twitterLoginRecordStore = twitterLoginRecordStore;
    }

    @EventListener
    public void handleEvent(CommitEvent ce) throws TwitterException {
        String repositoryName = ce.getRepositoryName();
        String changeSetId = ce.getChangeSetId();
        ChangesetDataFE csData = revisionDataService.getChangeset(repositoryName, changeSetId);
        if (csData != null) {
            String commitAuthor = csData.getAuthor();
            User user = committerUserMappingManager.getUserForCommitter(repositoryName, commitAuthor);
            if (user != null) {
                TwitterLoginRecord loginRecord = twitterLoginRecordStore.get(user.getUsername());
                if (loginRecord != null) {
                    postToTwitter(loginRecord, csData);
                } else {
                    LOGGER.info("Twitter account not set for user {}", user);
                }
            } else {
                LOGGER.info("Can't find a user name for commit author {} in repository {}", commitAuthor, repositoryName);
            }
        } else {
            LOGGER.warn("Failed to find changeset data for {} in repository {}", changeSetId, repositoryName);
        }
    }

    /**
     * It's a pity we've got to create the factory on each call, but it's much easier than storing the factory
     * or the Twitter object in yet another store.
     */
    private void postToTwitter(TwitterLoginRecord loginRecord, ChangesetDataFE csData) {
        try {
            // TwitterFactory can be configured only via PropertyConfiguration
            Properties props = new Properties();
            props.put("oauth.consumerKey", loginRecord.getConsumerKey());
            props.put("oauth.consumerSecret", loginRecord.getConsumerSecret());
            props.put("oauth.accessToken", loginRecord.getToken());
            props.put("oauth.accessTokenSecret", loginRecord.getTokenSecret());
            TwitterFactory twitterFactory = new TwitterFactory(new PropertyConfiguration(props));

            Twitter twitter = twitterFactory.getInstance();
            twitter.verifyCredentials();
            twitter.updateStatus(csData.getComment());
        } catch (TwitterException exception) {
            LOGGER.error("Failed posting to Twitter", exception);
        }
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        eventPublisher.register(this);
    }

    @Override
    public void destroy() throws Exception {
        eventPublisher.unregister(this);
    }
}
