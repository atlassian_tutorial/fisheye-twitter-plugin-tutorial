package com.example.ampstutorial;

import com.atlassian.fecru.user.EffectiveUserProvider;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.google.common.collect.ImmutableMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.google.common.base.Strings.isNullOrEmpty;

public class TwitterSettingsServlet extends HttpServlet {

    private final EffectiveUserProvider effectiveUserProvider;
    private final TwitterLoginRecordStore twitterLoginRecordStore;
    private final TemplateRenderer templateRenderer;

    public TwitterSettingsServlet(EffectiveUserProvider effectiveUserProvider,
                                  TwitterLoginRecordStore twitterLoginRecordStore,
                                  TemplateRenderer templateRenderer) {
        this.effectiveUserProvider = effectiveUserProvider;
        this.twitterLoginRecordStore = twitterLoginRecordStore;
        this.templateRenderer = templateRenderer;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("decorator", "fisheye.userprofile.tab");
        response.setContentType("text/html");

        TwitterLoginRecord loginRecord = twitterLoginRecordStore.get(getCurrentUser());

        templateRenderer.render("/templates/twitterSettings.vm",
                loginRecord != null ? ImmutableMap.of("loginRecord", loginRecord) : ImmutableMap.of(),
                response.getWriter());
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String consumerKey = request.getParameter("consumerKey");
        String consumerSecret = request.getParameter("consumerSecret");
        String token = request.getParameter("token");
        String tokenSecret = request.getParameter("tokenSecret");

        if (       !isNullOrEmpty(consumerKey)
                && !isNullOrEmpty(consumerSecret)
                && !isNullOrEmpty(token)
                && !isNullOrEmpty(tokenSecret)) {
            storeLoginRecord(consumerKey, consumerSecret, token, tokenSecret);
        }
        response.sendRedirect("./twitter-settings");
    }

    private void storeLoginRecord(String consumerKey, String consumerSecret, String token, String tokenSecret) {
        twitterLoginRecordStore.put(getCurrentUser(),
                new TwitterLoginRecord(consumerKey, consumerSecret, token, tokenSecret));
    }

    private String getCurrentUser() {
        return effectiveUserProvider.getEffectiveUserLogin().getUserName();
    }

}
