package com.example.ampstutorial;

import java.util.HashMap;
import java.util.Map;

/**
 * Simple, non-persistent store of {@link TwitterLoginRecord}. Useful during development process, be should be replaced
 * with a persistent store for production use.
 */
public class TwitterLoginRecordStoreImpl implements TwitterLoginRecordStore {

    private final Map<String, TwitterLoginRecord> store = new HashMap<>();

    @Override
    public TwitterLoginRecord get(String userName) {
        return store.get(userName);
    }

    @Override
    public void put(String userName, TwitterLoginRecord record) {
        store.put(userName, record);
    }
}
