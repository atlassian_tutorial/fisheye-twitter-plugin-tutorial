package com.example.ampstutorial;

/**
 * Interface for storing twitter authentication tokens for users.
 * Implementations can decide if they should be backed by a persistent store or not.
 */
public interface TwitterLoginRecordStore {

    TwitterLoginRecord get(String userName);

    void put(String userName, TwitterLoginRecord record);
}
