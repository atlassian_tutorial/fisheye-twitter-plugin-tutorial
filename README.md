# FishEye Twitter Integration Plugin #

Example FishEye plugin to sends each of your commit messages to your Twitter account.  See [FishEye Twitter Integration Plugin Tutorial](https://developer.atlassian.com/fecrudev/tutorials/fisheye-twitter-integration-plugin-tutorial)